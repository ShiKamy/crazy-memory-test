//
//  ViewController.m
//  CrazyMemory
//
//  Created by kkk on 2021/2/20.
//

#import "ViewController.h"

#import "ViewC.h"
#import "ObjectC.h"

#import <objc/runtime.h>
#import <malloc/malloc.h>


@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSString *sad = @"asdd";
    
    NSString *sad1 = @"asdd1";
    NSString *sad2 = @"asdd2";
    NSString *sad3 = @"asdd3";
    NSString *sad4 = @"asdd4";
    NSString *sad5 = @"asdd5";
    NSString *sad6 = @"asdd6";
    NSString *sad7 = @"asdd7";
    NSString *sad8 = @"asdd8";
    NSString *sad9 = @"asdd9";
    NSString *sad10 = @"asdd10";
    NSString *sad11 = @"asdd11";
    NSString *sad12 = @"asdd";
    NSString *sad13 = @"asdd";
    NSString *sad14 = @"asdd";
    NSString *sad15 = @"asdd";
    NSString *sad16 = @"asdd";
    NSString *sad17 = @"asdd";
    NSString *sad18 = @"asdd";
    NSString *sad19 = @"asdd";
    NSString *sad20 = @"asdd";
    NSString *sad21 = @"asdd";
    NSString *sad22 = @"asdd";
    NSString *sad23 = @"asdd";
    NSString *sad24 = @"asdd";
    NSString *sad25 = @"asdd";
    NSString *sad26 = @"asdd";
    NSString *sad27 = @"asdd";
    NSString *sad28 = @"asdd";
    NSString *sad29 = @"asdd";
    NSString *sad30 = @"asdd";
    NSString *sad31 = @"asdd";
    NSString *sad32 = @"asdd";
    NSString *sad33 = @"asdd";
    NSString *sad35 = @"asdd";
    NSString *sad36 = @"asdd";
    NSString *sad37 = @"asdd";
    NSString *sad38 = @"asdd";
    NSString *sad39 = @"asdd";
    NSString *sad40 = @"asdd";
    NSString *sad41 = @"asdd";
    NSString *sad42 = @"asdd";
    NSString *sad43 = @"asdd";
    NSString *sad44 = @"asdd";
    NSString *sad45 = @"asdd";
    NSString *sad46 = @"asdd";
    NSString *sad47 = @"asdd";
    NSString *sad48 = @"asdd48";
    NSString *sad49 = @"asdd49";
    NSString *sad50 = @"asdd50";
    
#pragma mark - NSObject  子类的测试 1
    
//    Class cls = [ObjectC class];
//    void *obj = &cls;
//    [(__bridge id)obj objectDo];
    
#pragma mark - UIView  子类的测试 2
    // UIView的类独享 占内存 360，360/8 = 45个对象
    //  ViewTTT新增了一个属性ccc，它的前面距离isa起点360个字节， 目前已知isa内存地址，中间间隔360-8=352
    //  当前cls--isa的地址确定了。那么ccc的地址就是，isa的地址 加上44*8 = ccc的地址
    
    Class cls = [ViewC class];
    void *obj = &cls;
    [(__bridge id)obj viewDo];
    
    NSLog(@"%zu", class_getInstanceSize(cls));
    NSLog(@"%zu", malloc_size((__bridge const void *)(cls)));
    
    
    NSLog(@"%p   %p", sad,  cls);
    
    // 对象指针在栈空间。cls的isa内存地址：&cls
    NSLog(@"%p   %p", &sad,  &cls);

    
//    unsigned int count;
//    Ivar *ivar = class_copyIvarList([UIView class], &count);
//    for (int i = 0; i < count; i++) {
//        Ivar iv = ivar[i];
//        const char *name = ivar_getName(iv);
//        NSString *strName = [NSString stringWithUTF8String:name];
//
//        NSLog(@"%@", strName);
//
//    }
//    free(ivar);
}

@end
