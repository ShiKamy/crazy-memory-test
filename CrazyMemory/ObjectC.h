//
//  ObjectC.h
//  CrazyMemory
//
//  Created by kkk on 2021/2/20.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ObjectC : NSObject

@property (nonatomic, strong) NSString *occc;

- (void)objectDo;

@end

NS_ASSUME_NONNULL_END
